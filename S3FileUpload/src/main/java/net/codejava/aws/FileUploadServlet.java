 package net.codejava.aws;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * Servlet implementation class FileUploadServlet
 */
@WebServlet("/upload")
@MultipartConfig(
		fileSizeThreshold = 1024 * 1024,
		maxFileSize = 1024 * 1024 * 10,
		maxRequestSize = 1024 * 1024 * 11
		)
public class FileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

 
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String description = request.getParameter("description");
		System.out.println("Description: " + description);
		
		Part part = request.getPart("file");
 		
		
		String fileName = getFileName(part);
		String accessKey = "AKIAXORNM4HBINI7DIR7";
		String secretKey = "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";
		String region = "ap-south-1";
		System.out.println("fileName: " + fileName);
		
		String message = "";
		try {
			//S3Util.uploadFile(fileName, part.getInputStream());
			S3util.uploadFile(fileName, part.getInputStream(),accessKey,secretKey,region);
			message = "Your file has been uploaded successfully.";
		}catch (Exception ex) {
			message = "Error uploading file: "+ ex.getMessage();
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("message.jsp").forward(request, response);
		 
	}
	private String getFileName(Part part) {
		String contentDisposition = part.getHeader("content-disposition");
		int beginIndex = contentDisposition.indexOf("filename=") + 10;
		int endIndex = contentDisposition.length() - 1;
		return contentDisposition.substring(beginIndex, endIndex);
	}
   
}
