 package net.codejava.aws;

import java.io.IOException;
import java.io.InputStream;

//import javax.swing.plaf.synth.Region;

import software.amazon.awssdk.auth.credentials.*;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Exception;

public class S3util {
	private static String BUCKET = "ecommerce-aplication-images";

	public static void uploadFile(String filename, InputStream inputStream, String accessKey, String secretKey, String region) throws S3Exception, AwsServiceException, SdkClientException, IOException {
		
		S3Client client = S3Client.builder()
			.region(Region.of(region))
			.credentialsProvider(
				StaticCredentialsProvider.create(
					AwsBasicCredentials.create(accessKey, secretKey)
				)
			)
			.build();
		
		PutObjectRequest request = PutObjectRequest.builder()
			.bucket(BUCKET)
			.key(filename)
//			.acl("public-read")
			.build();

		client.putObject(request, RequestBody.fromInputStream(inputStream, inputStream.available()));
	}
}

















//package net.codejava.aws;
// 
//import java.io.IOException;
//import java.io.InputStream;
//
//import javax.swing.plaf.synth.Region;
//
//import software.amazon.awssdk.awscore.exception.AwsServiceException;
//import software.amazon.awssdk.core.exception.SdkClientException;
//import software.amazon.awssdk.core.sync.RequestBody;
//import software.amazon.awssdk.services.s3.S3Client;
//import software.amazon.awssdk.services.s3.model.PutObjectRequest;
//import software.amazon.awssdk.services.s3.model.S3Exception;
//
//public class S3Util {
//	private static String BUCKET = "ecommerce-product-images-praveen";
//
//	public static void uploadFile(String filename, InputStream inputStream) throws S3Exception, AwsServiceException, SdkClientException, IOException {
//		
//		S3Client client = S3Client.builder().build();
//		
//		PutObjectRequest request = PutObjectRequest.builder()
//												.bucket(BUCKET)
//												.key(filename)
//												.build();
// 
//
//		client.putObject(request, RequestBody.fromInputStream(inputStream, inputStream.available()));
//	}
//}
