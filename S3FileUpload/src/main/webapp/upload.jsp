<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>s3 File Upload Example</title>
</head>
<body style="background-color: #f1f1f1;">
<div align="center">
	<div style="background-color: #333; color: white; padding: 10px;"><h1>S3 File Upload Demo</h1></div>
</div>
<form action="upload" method="post" enctype="multipart/form-data" style="margin-top: 20px;" align="center">
	<p style="margin-bottom: 10px;">Description: <input type="text" name="description" size="30" required style="padding: 5px; border-radius: 3px; border: none; box-shadow: 1px 1px 2px #ccc;"></p>
	<p style="margin-bottom: 10px;"><input type="file" name="file" required style="padding: 5px; border-radius: 3px; border: none; box-shadow: 1px 1px 2px #ccc;"></p>
	<p><button type="submit" style="background-color: #4CAF50; color: white; padding: 8px 16px; border: none; border-radius: 3px; cursor: pointer;">Submit</button></p>
</form>
</body>
</html>